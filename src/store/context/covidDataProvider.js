import React, { useReducer } from "react";
import Context, { initalState } from "./covidStore";

import { reducers } from "./reducer";

const CovidDataProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducers, initalState),
    value = { state, dispatch };

  return <Context.Provider value={value}>{children}</Context.Provider>;
};

export default CovidDataProvider;
