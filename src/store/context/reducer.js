export function reducers(state, action) {
  switch (action.type) {
    case "SET_DATA":
      state.mapData = action.mapData;
      return { ...state };

    default:
      return state;
  }
}
