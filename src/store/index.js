import {applyMiddleware, createStore} from 'redux';

import thunk from 'redux-thunk';
import {composeWithDevTools} from 'redux-devtools-extension/logOnlyInProduction';
import injectClient from './injectClient';

import reducer from '../reducer';

// const storeProd = createStore(reducer, composeWithDevTools(applyMiddleware(promise(), thunk)));
const storeLocal = (axios) =>
  createStore(
    reducer,
    composeWithDevTools(applyMiddleware(injectClient(axios), thunk)),
  );

export default storeLocal;
