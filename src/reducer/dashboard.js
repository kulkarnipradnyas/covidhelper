const initialState = {
  isMenuOpen: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case 'IS_MENU_OPEN':
      return {...state, isMenuOpen: action.payload};
    default:
      return {...state};
  }
};
