import Constant from '../constants';

const initialState = {
  isLoggedIn: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case Constant.LOGGED_IN:
      return {...state, isLoggedIn: action.payload};
    default:
      return {...state};
  }
};
