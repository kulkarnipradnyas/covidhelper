import React, { useState, useContext, useEffect } from "react";
import { StyleSheet, View, Text, TouchableOpacity, Alert } from "react-native";
import {
  Table,
  TableWrapper,
  Col,
  Cols,
  Cell
} from "react-native-table-component";

import { connect } from "react-redux";
import Context from "../../store/context/covidStore";
import { createList, getStateWiseData } from "../../actions/covid";
import { SearchBar, DataItem, Empty, Seperator, Loader } from "../../component";
import Header from "./header";

/**
 * Component for Header
 * @component
 * @param {*} props
 */
const Dashboard = props => {
  const india = !!(props.route.params && props.route.params.india);
  const [loading, setLoading] = useState(true),
    [dataList, setDataList] = useState([]),
    [titles, setTitles] = useState([]),
    { userstate, dispatch } = useContext(Context);

  const _alertIndex = value => {
    Alert.alert(`This is column ${value}`);
  };
  const elementButton = value => (
    <TouchableOpacity onPress={() => _alertIndex(value)}>
      <View style={styles.btn}>
        <Text style={styles.btnText}>button</Text>
      </View>
    </TouchableOpacity>
  );

  const fetchData = async () => {
    setLoading(true);
    let statusList = await getStateWiseData();
    console.log(statusList);
    dispatch({ type: "SET_DATA", mapData: statusList.data });
    setTitles(statusList.titles);
    setDataList(statusList.data);
    setLoading(false);
  };

  useEffect(() => {
    debugger;
    fetchData();
  }, [india]);

  const searchByName = text => {
    let filteredList = originalList.filter(item => {
      const itemData = item.country.toUpperCase();
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    setDataList(filteredList);
  };
  console.log(titles);
  return (
    <View style={styles.container}>
      <Table style={{ flexDirection: "row" }} borderStyle={{ borderWidth: 1 }}>
        {/* Left Wrapper */}
        <TableWrapper style={{ width: 90 }}>
          <Cell data="" style={styles.singleHead} />
          <TableWrapper style={{ flexDirection: "row" }}>
            <Col
              data={titles}
              style={styles.title}
              heightArr={titles.map(() => 30)}
              textStyle={styles.titleText}
            ></Col>
          </TableWrapper>
        </TableWrapper>

        {/* Right Wrapper */}
        <TableWrapper style={{ flex: 1 }}>
          <Cols
            data={dataList}
            heightArr={titles.map(() => 30)}
            textStyle={styles.text}
          />
        </TableWrapper>
      </Table>
    </View>
  );
};

const styles = StyleSheet.create({
  container: { flex: 1, padding: 16, paddingTop: 30, backgroundColor: "#fff" },
  singleHead: { width: 90, height: 30, backgroundColor: "#c8e1ff" },
  head: { flex: 1, backgroundColor: "#c8e1ff" },
  title: { flex: 0, backgroundColor: "#f6f8fa" },
  titleText: { marginRight: 6, textAlign: "center" },
  text: { textAlign: "center" },
  btn: {
    width: 58,
    height: 18,
    marginLeft: 15,
    backgroundColor: "#c8e1ff",
    borderRadius: 2
  },
  btnText: { textAlign: "center" }
});
const mapStateToProps = ({ dashboard }) => {
  return { isMenuOpen: dashboard.isMenuOpen };
};

export default connect(mapStateToProps, null)(Dashboard);
