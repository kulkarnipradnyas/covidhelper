import React, { useState, useContext, useEffect } from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import styles from "../../global/styles";

const Header = ({ state, setDataList, originalList }) => {
  return (
    <View style={styles.main.header}>
      <Text style={styles.main.sortTitle}>{state.language.sort.label}: </Text>

      <TouchableOpacity
        style={{ flexDirection: "row", alignItems: "center" }}
        onPress={() => setDataList([...originalList[1].reverse()])}
      >
        <Image
          source={require("../../assets/sort.png")}
          style={[styles.main.sort.image, { tintColor: "orange" }]}
        />
        <Text style={[styles.main.sort.title, { color: "orange" }]}>
          {state.language.sort.sort[0]}
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={{ flexDirection: "row", alignItems: "center" }}
        onPress={() => setDataList([...originalList[2].reverse()])}
      >
        <Image
          source={require("../../assets/sort.png")}
          style={[styles.main.sort.image, { tintColor: "red" }]}
        />
        <Text style={[styles.main.sort.title, { color: "red" }]}>
          {state.language.sort.sort[1]}
        </Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={{ flexDirection: "row", alignItems: "center" }}
        onPress={() => setDataList([...originalList[3].reverse()])}
      >
        <Image
          source={require("../../assets/sort.png")}
          style={[styles.main.sort.image, { tintColor: "green" }]}
        />
        <Text style={[styles.main.sort.title, { color: "green" }]}>
          {state.language.sort.sort[2]}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

export default Header;
