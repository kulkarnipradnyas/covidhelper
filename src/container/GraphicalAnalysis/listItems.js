import React from "react";
import moment from "moment";
import numeral from "numeral";
import { View, Text } from "react-native";
import styles from "../../global/styles";

export const ListItem = ({ item, state }) => {
  return (
    <View style={{ padding: 5 }}>
      <View style={{ flexDirection: "row", alignItems: "center" }}>
        <View
          style={{
            backgroundColor: "rgb(242, 63, 52)",
            width: 8,
            height: 8,
            borderRadius: 8,
            margin: 5,
            marginLeft: 0
          }}
        />
        <Text style={styles.detail.listItem.date}>
          {moment(item.time).format("dddd DD/MM (hh:mm)")}
        </Text>
      </View>

      <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
        <Text style={styles.detail.listItem.label}>
          {state.language.detail.total}:{" "}
          <Text style={styles.detail.listItem.value}>
            {numeral(item.total).format("0,0")}
          </Text>
        </Text>
        <Text style={styles.detail.listItem.label}>
          {state.language.detail.rec}:{" "}
          <Text style={styles.detail.listItem.value}>
            {numeral(item.recovered).format("0,0")}
          </Text>
        </Text>
        <Text style={styles.detail.listItem.label}>
          {state.language.detail.death}:{" "}
          <Text style={styles.detail.listItem.value}>
            {numeral(item.death).format("0,0")}
          </Text>
        </Text>
      </View>
    </View>
  );
};
