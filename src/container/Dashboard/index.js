import React, { useState, useContext, useEffect } from "react";
import { SafeAreaView, FlatList } from "react-native";
import { connect } from "react-redux";
import styles from "../../global/styles";
import Context from "../../store/context/covidStore";
import { createList, getStateWiseData } from "../../actions/covid";
import { SearchBar, DataItem, Empty, Seperator, Loader } from "../../component";
import Header from "./header";

/**
 * Component for Header
 * @component
 * @param {*} props
 */
const Dashboard = props => {
  const india = !!(props.route.params && props.route.params.india);
  const [loading, setLoading] = useState(true),
    [dataList, setDataList] = useState([]),
    { state, dispatch } = useContext(Context),
    [originalList, setOriginalList] = useState([]);

  const fetchData = async () => {
    setLoading(true);
    let statusList = india ? await getStateWiseData() : await createList();
    const mapList = india ? statusList : statusList[0];
    dispatch({ type: "SET_DATA", mapData: mapList });
    setOriginalList(statusList);
    setDataList(mapList);
    setLoading(false);
  };

  useEffect(() => {
    debugger;
    fetchData();
  }, [india]);

  const renderCountries = ({ item }) => (
    <DataItem
      item={item}
      onPress={() =>
        props.navigation.navigate("graphicalAnalysis", {
          data: JSON.stringify(item)
        })
      }
    />
  );
  const searchByName = text => {
    let filteredList = originalList[0].filter(item => {
      const itemData = item.country.toUpperCase();
      const textData = text.toUpperCase();

      return itemData.indexOf(textData) > -1;
    });

    setDataList(filteredList);
  };
  return (
    <SafeAreaView style={styles.mainContainer}>
      <SearchBar label={state.language.search} onSearch={searchByName} />
      {loading && dataList && !dataList.length ? (
        <Loader />
      ) : (
        <FlatList
          data={dataList}
          refreshing={loading}
          initialNumToRender={10}
          onRefresh={fetchData}
          renderItem={renderCountries}
          ListEmptyComponent={() => <Empty />}
          ListHeaderComponent={() => (
            <Header
              state={state}
              setDataList={setDataList}
              originalList={originalList}
            />
          )}
          contentContainerStyle={{ flexGrow: 1 }}
          ItemSeparatorComponent={() => <Seperator />}
          keyExtractor={(_, index) => index.toString()}
        />
      )}
    </SafeAreaView>
  );
};
const mapStateToProps = ({ dashboard }) => {
  return { isMenuOpen: dashboard.isMenuOpen };
};

export default connect(mapStateToProps, null)(Dashboard);
