import React from 'react';
import {connect} from 'react-redux';
import {Text} from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import India from '../Statastic/india'
import World from '../Statastic/world'
import { createStackNavigator } from "@react-navigation/stack";
import HeaderView from '../../component/HeaderView'
import CovidDashboard from '../CovidDashboard'


const Stack = createStackNavigator();

const Tab = createMaterialTopTabNavigator();
//const CovidInfo = (props) => <Text>adassaf</Text>

const CovidInfo = props => (
    <Stack.Navigator
      initialRouteName="india"
      screenOptions={{
        header: ()=><HeaderView title="Live Data"/>
      }}
    >
      <Stack.Screen name="india" component={CovidDashboard} />
    </Stack.Navigator>
  );

// const CovidInfo = (props) =>  (<Tab.Navigator>
// <Tab.Screen name="world" component={World} />
// <Tab.Screen name="india" component={India} />
// </Tab.Navigator>)

export default (CovidInfo);
