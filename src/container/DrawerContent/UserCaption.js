import React from 'react';
import {View} from 'react-native';
import {Avatar, Title, Caption, Text} from 'react-native-paper';

const UserCaption = (props) => {
  const style = {
    color: props.theme.dark
      ? props.theme.lightTheme.text
      : props.theme.darkTheme.text,
  };
  return props.isLoggedIn ? (
    <View>
      <View style={{flexDirection: 'row', marginTop: 15}}>
        <Avatar.Image
          source={{
            uri:
              'https://avatars1.githubusercontent.com/u/32020176?s=400&u=768adfbde2c460e34b064719844c4c773746ea42&v=4',
          }}
          size={50}
        />
        <View style={{flexDirection: 'column', marginLeft: 15}}>
          <Title
            style={{
              color: props.theme.dark
                ? props.theme.lightTheme.text
                : props.theme.darkTheme.text,
            }}>
            Dummy User
          </Title>
          <Caption
            style={{
              color: props.theme.dark
                ? props.theme.lightTheme.text
                : props.theme.darkTheme.text,
            }}>
            phone number
          </Caption>
        </View>
      </View>
    </View>
  ) : (
    <View>
      <Text style={style}>
        <Text style={style} onPress={() => props.navigation.navigate('signUp')}>
          {' '}
          signUp{' '}
        </Text>{' '}
        Or{' '}
        <Text style={style} onPress={() => props.navigation.navigate('signIn')}>
          {' '}
          Sign In
        </Text>
      </Text>
    </View>
  );
};

export default UserCaption;
