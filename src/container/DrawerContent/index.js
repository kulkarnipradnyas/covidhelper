import React from 'react';
import {connect} from 'react-redux';
import DrawerComponent from './DrawerComponent';
/**
 * Component for Header
 * @component
 * @param {*} props
 */
const DrawerContent = (props) => {
  return <DrawerComponent {...props} />;
};
const mapStateToProps = ({dashboard}) => {
  return {isMenuOpen: dashboard.isMenuOpen};
};
const mapDispatchToProps = {};
export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent);
