import React, { useState } from "react";
import { View } from "react-native";
import {
  Avatar,
  Title,
  Caption,
  Drawer,
  Text,
  TouchableRipple,
  Switch,
  withTheme
} from "react-native-paper";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import { styles } from "./style";
import { Icon } from "react-native-elements";
import UserCaption from "./UserCaption";

function DrawerContent(props) {
  const [isDarkTheme, setDarkTheme] = useState(true);

  const toggleTheme = () => {
    setDarkTheme(!isDarkTheme);
    props.theme.dark = isDarkTheme;
  };

  return (
    <View
      // eslint-disable-next-line react-native/no-inline-styles
      style={{
        flex: 1,
        backgroundColor: props.theme.dark
          ? props.theme.lightTheme.background
          : props.theme.darkTheme.background
      }}
    >
      <DrawerContentScrollView {...props}>
        <View style={styles.drawerContent}>
          <View style={styles.userInfoSection}>
            <UserCaption {...props} />
          </View>
          <Drawer.Section style={styles.drawerSection}>
            <DrawerItem
              icon={({ color, size }) => (
                <Icon
                  name="user"
                  type="font-awesome"
                  color={
                    props.theme.dark
                      ? props.theme.lightTheme.text
                      : props.theme.darkTheme.text
                  }
                  size={size}
                />
              )}
              label="User Profile"
              labelStyle={{
                color: props.theme.dark
                  ? props.theme.lightTheme.text
                  : props.theme.darkTheme.text
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon
                  name="bookmark"
                  type="font-awesome"
                  color={
                    props.theme.dark
                      ? props.theme.lightTheme.text
                      : props.theme.darkTheme.text
                  }
                  size={size}
                />
              )}
              label="Covid Worldwide Data"
              labelStyle={{
                color: props.theme.dark
                  ? props.theme.lightTheme.text
                  : props.theme.darkTheme.text
              }}
              onPress={() =>
                props.navigation.navigate("dashboard", {
                  india: false
                })
              }
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon
                  name="bookmark"
                  type="font-awesome"
                  color={
                    props.theme.dark
                      ? props.theme.lightTheme.text
                      : props.theme.darkTheme.text
                  }
                  size={size}
                />
              )}
              label="Covid Inida Live"
              labelStyle={{
                color: props.theme.dark
                  ? props.theme.lightTheme.text
                  : props.theme.darkTheme.text
              }}
              onPress={() => props.navigation.navigate("indiaDashboard")}
            />
          </Drawer.Section>
          <Drawer.Section style={[styles.drawerSection]}>
            <Caption
              style={[
                {
                  color: props.theme.dark
                    ? props.theme.lightTheme.text
                    : props.theme.darkTheme.text,
                  marginLeft: 18,
                  fontSize: 14
                },
                ,
              ]}
            >
              Preference
            </Caption>
            <TouchableRipple
              onPress={() => {
                toggleTheme();
              }}
            >
              <View style={styles.preference}>
                <Text
                  style={{
                    color: props.theme.dark
                      ? props.theme.lightTheme.text
                      : props.theme.darkTheme.text
                  }}
                >
                  Dark Theme
                </Text>
                <View pointerEvents="none">
                  <Switch
                    value={isDarkTheme}
                    color={
                      !props.theme.dark
                        ? props.theme.lightTheme.text
                        : props.theme.darkTheme.text
                    }
                  />
                </View>
              </View>
            </TouchableRipple>
          </Drawer.Section>
        </View>
      </DrawerContentScrollView>
      <Drawer.Section style={styles.bottomDrawerSection}>
        <DrawerItem
          label="Sign Out"
          labelStyle={{
            color: props.theme.dark
              ? props.theme.lightTheme.text
              : props.theme.darkTheme.text
          }}
          icon={({ color, size }) => (
            <Icon
              name="exit-to-app"
              color={
                props.theme.dark
                  ? props.theme.lightTheme.text
                  : props.theme.darkTheme.text
              }
              size={size}
            />
          )}
        />
      </Drawer.Section>
    </View>
  );
}

export default withTheme(DrawerContent);
