import React from "react";
import { connect } from "react-redux";
import { Text } from "react-native";
import CovidCardboard from "./covidCardboard";

const CovidDashboard = props => <CovidCardboard />;
const mapStateToProps = ({ dashboard }) => {
  return { isMenuOpen: dashboard.isMenuOpen };
};
const mapDispatchToProps = {};
export default connect(mapStateToProps, mapDispatchToProps)(CovidDashboard);
