import React, { useState, Component } from "react";
import { Text, Button, Card, Icon } from "react-native-elements";
import { TabView, SceneMap } from "react-native-tab-view";
import { View, StyleSheet } from "react-native";
import india from "../Statastic/india";
import world from "../Statastic/world";

const FirstRoute = () => (
  <View style={[styles.scene, { backgroundColor: "#ffffff" }]} />
);

const SecondRoute = () => (
  <View style={[styles.scene, { backgroundColor: "#ffffff" }]} />
);

const CovidCardboard = () => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: "first", title: "Inida" },
    { key: "second", title: "World" }
  ]);

  const renderScene = SceneMap({
    first: india,
    second: world
  });

  return (
    <View style={styles.tabContainer}>
      <TabView
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        style={styles.tabView}
        //initialLayout={initialLayout}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  scene: {
    flex: 1
  },
  tabContainer: {
    height: 250,
    margin: 20,
    elevation: 2,
    overflow: "hidden",
    shadowColor: "black",
    shadowRadius: 5,
    shadowOpacity: 1,
    borderRadius: 4
  },
  tabView: {
    color: "purple",
    shadowColor: "black",
    borderRadius: 4
  }
});

export default CovidCardboard;
