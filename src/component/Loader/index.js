import React from "react";
import { View } from "react-native";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import styles from "../../global/styles";

export const Loader = () => {
  return [...Array(10)].map((_, i) => {
    return (
      <SkeletonPlaceholder {...styles.loadingTheme} key={i}>
        <View
          style={{
            borderBottomWidth: 1,
            borderColor: "#eceff1",
            paddingVertical: 10
          }}
        >
          <View style={styles.main.loading.topContainer}>
            <View style={styles.main.loading.topLeft} />
            <View style={styles.main.loading.topLeft} />
          </View>
          <View style={styles.main.loading.bottom} />
        </View>
      </SkeletonPlaceholder>
    );
  });
};
