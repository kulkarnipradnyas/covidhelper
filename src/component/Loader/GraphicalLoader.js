import React from "react";
import { View } from "react-native";
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import styles from "../../global/styles";

export const GraphicalLoader = () => {
  return (
    <SkeletonPlaceholder {...styles.loadingTheme}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          marginTop: 10,
          marginBottom: 20
        }}
      >
        <View style={styles.detail.loading.headerLeft} />
        <View style={styles.detail.loading.headerRight} />
      </View>
      <View style={styles.detail.loading.graph}>
        {[140, 120, 180, 140, 160, 150, 120].map(h => (
          <View style={{ width: 50, height: h }} />
        ))}
      </View>
      <View style={styles.detail.loading.legend} />

      {[...Array(8)].map((_, i) => {
        return (
          <View key={i} style={{ padding: 5 }}>
            <View style={styles.detail.loading.listTop} />
            <View style={styles.detail.loading.listBottom} />
          </View>
        );
      })}
    </SkeletonPlaceholder>
  );
};
