import React from "react";
import { View, Image, Text } from "react-native";
import styles from "../../global/styles";

export const Empty = ({ loading }) => {
  return loading ? (
    <View style={{ flex: 1 }} />
  ) : (
    <View style={styles.center}>
      <Image
        source={require("../../assets/search.png")}
        style={styles.main.noResult.image}
      />
      <Text style={styles.main.noResult.title}>{"No result Found!!"}</Text>
    </View>
  );
};
