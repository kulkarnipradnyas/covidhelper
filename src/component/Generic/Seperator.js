import React from "react";
import { View } from "react-native";
import styles from "../../global/styles";

export const Seperator = () => <View style={styles.main.seperator} />;
