import {DefaultTheme} from 'react-native-paper';

const theme = () => ({
  ...DefaultTheme,
  dark: false,
  lightTheme: {
    ...DefaultTheme.colors,
    primary: 'tomato',
    accent: 'black',
    background: '#fff',
  },
  darkTheme: {
    ...DefaultTheme.colors,
    primary: 'tomato',
    accent: 'black',
    background: '#2b2a2a',
    text: '#fff',
    onBackground: '#fff',
  },
});

export default theme;
