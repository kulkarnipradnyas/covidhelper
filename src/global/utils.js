export const Utils = {
  validatePhoneNumber: (phone) => {
    var regexp = /^\+[0-9]?()[0-9](\s|\S)(\d[0-9]{8,16})$/;
    return regexp.test(phone);
  },
};
