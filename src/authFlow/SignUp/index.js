import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Input, Button} from 'react-native-elements';
import {Utils} from '../../global/utils';
//import firebase from 'react-native-firebase';

const SignUp = (props) => {
  const [state, setState] = useState({
    username: '',
    password: '',
    email: '',
    phoneNumber: '',
  });
  const onChangeText = (key, val) => {
    setState({state, [key]: val});
  };
  const handleSignup = async () => {
    const {username, password, email, phoneNumber} = state;
    try {
      const isValid = await Utils.validatePhoneNumber(phoneNumber);
      if (isValid && username && password) {
        console.log('user successfully signed up!: ', 'success');
        // do server call to check authentication of number and send otp
        //  props.navigation.navigate('phoneAuth');
      }
      // here place your signup logic

    } catch (err) {
      console.log('error signing up: ', err);
    }
  };

  return (
    <View style={styles.container}>
      <Input
        inputStyle={styles.input}
        placeholder="Username"
        autoCapitalize="none"
        placeholderTextColor="black"
        onChangeText={(val) => onChangeText('username', val)}
      />
      <Input
        inputStyle={styles.input}
        placeholder="Password"
        secureTextEntry={true}
        autoCapitalize="none"
        placeholderTextColor="black"
        onChangeText={(val) => onChangeText('password', val)}
      />
      <Input
        inputStyle={styles.input}
        placeholder="Email"
        autoCapitalize="none"
        placeholderTextColor="black"
        onChangeText={(val) => onChangeText('email', val)}
      />
      <Input
        inputStyle={styles.input}
        placeholder="Phone Number with country code"
        autoCapitalize="none"
        placeholderTextColor="black"
        onChangeText={(val) => onChangeText('phoneNumber', val)}
        onBlur={(value) => Utils.validatePhoneNumber(value)}
      />
      <Button
        buttonStyle={styles.button}
        title="Sign Up"
        onPress={() => handleSignup()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    width: 350,
    height: 55,
    backgroundColor: '#ffffff',
    margin: 10,
    padding: 8,
    color: 'black',
    borderRadius: 14,
    fontSize: 18,
    fontWeight: '500',
    borderWidth: 0,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#42A5F5',
  },
  button: {
    padding: 10,
    width: 200,
    borderRadius: 14,
    backgroundColor: '#9400D3',
  },
});

export default SignUp;
