import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {Input, Button} from 'react-native-elements';

const SignIn = (props) => {
  const [state, setState] = useState({
    phoneNumber: '',
    password: '',
  });
  const onChangeText = (key, val) => {
    setState({state, [key]: val});
  };
  const signUp = async () => {
    const {phoneNumber, password, email, phone_number} = state;
    try {
      props.navigation.navigate('phoneAuth');
      // here place your signup logic
      console.log('user successfully signed up!: ', 'success');
    } catch (err) {
      console.log('error signing up: ', err);
    }
  };

  return (
    <View style={styles.container}>
      <Input
        inputStyle={styles.input}
        placeholder="Phone Number"
        autoCapitalize="none"
        placeholderTextColor="black"
        onChangeText={(val) => onChangeText('phoneNumber', val)}
      />
      <Input
        inputStyle={styles.input}
        placeholder="Password"
        secureTextEntry={true}
        autoCapitalize="none"
        placeholderTextColor="black"
        onChangeText={(val) => onChangeText('password', val)}
      />
      <Button
        buttonStyle={styles.button}
        title="Sign In"
        onPress={() => signUp()}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    width: 350,
    height: 55,
    backgroundColor: '#ffffff',
    margin: 10,
    padding: 8,
    color: 'black',
    borderRadius: 14,
    fontSize: 18,
    fontWeight: '500',
    borderWidth: 0,
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#42A5F5',
  },
  button: {
    padding: 10,
    width: 200,
    borderRadius: 14,
    backgroundColor: '#9400D3',
  },
});

export default SignIn;
