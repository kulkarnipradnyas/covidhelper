import React, {Component} from 'react';
import {StyleSheet, SafeAreaView, TouchableOpacity, View} from 'react-native';
import {Text, Input} from 'react-native-elements';
//import firebase from 'react-native-firebase';

class PhoneAuthScreen extends Component {
  state = {
    phone: '',
    confirmResult: null,
    verificationCode: '',
    userId: '',
  };
  validatePhoneNumber = () => {
    var regexp = /^\+[0-9]?()[0-9](\s|\S)(\d[0-9]{8,16})$/;
    return regexp.test(this.state.phone);
  };

  handleSendCode = () => {
    // Request to send OTP
    if (this.validatePhoneNumber()) {
      firebase
        .auth()
        .signInWithPhoneNumber(this.state.phone)
        .then((confirmResult) => {
          this.setState({confirmResult});
        })
        .catch((error) => {
          alert(error.message);
          console.log(error);
        });
    } else {
      alert('Invalid Phone Number');
    }
  };

  changePhoneNumber = () => {
    this.setState({confirmResult: null, verificationCode: ''});
  };

  handleVerifyCode = () => {
    // Request for OTP verification
    const {confirmResult, verificationCode} = this.state;
    if (verificationCode.length === 6) {
      confirmResult
        .confirm(verificationCode)
        .then((user) => {
          this.setState({userId: user.uid});
          alert(`Verified! ${user.uid}`);
        })
        .catch((error) => {
          alert(error.message);
          console.log(error);
        });
    } else {
      alert('Please enter a 6 digit OTP code.');
    }
  };

  renderConfirmationCodeView = () => {
    return (
      <View style={styles.verificationView}>
        <Input
          inputStyle={styles.input}
          placeholder="Verification code"
          placeholderTextColor="black"
          keyboardType="numeric"
          onChangeText={(verificationCode) => {
            this.setState({verificationCode});
          }}
          maxLength={6}
        />
        <TouchableOpacity
          style={[styles.themeButton, {marginTop: 20}]}
          onPress={this.handleVerifyCode}>
          <Text>Verify Code</Text>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <SafeAreaView style={[styles.container, {backgroundColor: '#333'}]}>
        <View style={styles.page}>{this.renderConfirmationCodeView()}</View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#42A5F5',
  },
  page: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#42A5F5',
  },
  input: {
    marginTop: 20,
    borderColor: '#555',
    borderRadius: 14,
    paddingLeft: 10,
    width: 350,
    height: 55,
    backgroundColor: '#ffffff',
    margin: 10,
    padding: 8,
    color: 'black',
    fontSize: 18,
    fontWeight: '500',
    borderWidth: 0,
  },
  themeButton: {
    width: 200,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: '#555',
    borderWidth: 2,
    borderRadius: 14,
    padding: 10,
    backgroundColor: '#9400D3',
  },
  verificationView: {
    width: '100%',
    alignItems: 'center',
    marginTop: 50,
  },
});

export default PhoneAuthScreen;
