import React, { useEffect } from "react";
import { Provider } from "react-redux";
import { Provider as ThemeProvider, DefaultTheme } from "react-native-paper";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import configureStore from "./src/store";
import DrawerContent from "./src/container/DrawerContent";
import Dashboard from "./src/container/Dashboard";
import axios from "axios";
import SignUp from "./src/authFlow/SignUp";
import SignIn from "./src/authFlow/SignIn";
import PhoneAuthScreen from "./src/authFlow/PhoneAuthentication";
import CovidInfo from "./src/container/CovidInfo";
import CovidDataProvider from "./src/store/context/covidDataProvider";
import GraphicalAnalysis from "./src/container/GraphicalAnalysis";
import IndiaDashboard from "./src/container/IndiaDashboard";
import { NovelCovid } from "novelcovid";

const track = new NovelCovid();

const store = configureStore(axios);
let navigatior = null;

const Stack = createStackNavigator();

const theme = {
  ...DefaultTheme,
  dark: false,
  lightTheme: {
    ...DefaultTheme.colors,
    primary: "tomato",
    accent: "black",
    background: "#fff"
  },
  darkTheme: {
    ...DefaultTheme.colors,
    primary: "tomato",
    accent: "black",
    background: "#2b2a2a",
    text: "#fff",
    onBackground: "#fff"
  }
};

const App = props => {
  const Drawer = createDrawerNavigator();

  return (
    <Provider store={store}>
      <CovidDataProvider>
        <ThemeProvider theme={theme}>
          <NavigationContainer ref={ref => (navigatior = ref)}>
            <Drawer.Navigator
              drawerType="slide"
              drawerPosition="left"
              drawerContent={props => <DrawerContent {...props} />}
              initialRouteName="dashboard"
            >
              <Drawer.Screen name="dashboard" component={Dashboard} />
              <Drawer.Screen name="indiaDashboard" component={IndiaDashboard} />
              <Drawer.Screen name="signUp" component={SignUp} />
              <Drawer.Screen name="signIn" component={SignIn} />
              <Drawer.Screen name="phoneAuth" component={PhoneAuthScreen} />
              <Drawer.Screen name="covidInfo" component={CovidInfo} />
              <Drawer.Screen
                name="graphicalAnalysis"
                component={GraphicalAnalysis}
              />
            </Drawer.Navigator>
          </NavigationContainer>
        </ThemeProvider>
      </CovidDataProvider>
    </Provider>
  );
};

export default App;
